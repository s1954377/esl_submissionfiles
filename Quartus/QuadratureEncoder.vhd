library ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.math_real.ALL;

ENTITY QuadratureEncoder is
    GENERIC(
		  DATA_WIDTH : natural := 32
    );
    PORT (
        clk   : IN  std_logic;
	rst   : in  std_logic;
        encA  : IN  std_logic;
        encB  : IN  std_logic;
        output : OUT std_logic_vector(DATA_WIDTH-1 downto 0)
);
end QuadratureEncoder;

architecture behavior of QuadratureEncoder is
	constant max_pos: NATURAL  := (2 ** 16) - 1;
     
begin
    process(clk)
	
        variable position: NATURAL range 0 to max_pos;
	variable encA_state: std_logic_vector (1 downto 0) := (others => '0');
	variable encB_state: std_logic_vector (1 downto 0) := (others => '0');
	variable direction: std_logic;
    begin
        IF(rising_edge(clk)) THEN
		encA_state := encA_state(0) & encA;
		encB_state := encB_state(0) & encB;
            IF(((encA_state(0) XOR encA_state(1)) OR (encB_state(0) XOR encB_state(1))) = '1') THEN -- input changed
                direction := encB_state(1) XOR encA_state(0);

                 -- update direction & position
                IF(direction = '1') THEN
                    IF(position + 1 >= max_pos) THEN
                        position := 0;
                    ELSE
                        position := position + 1;
                    END IF;
                ELSE
                    IF(position > 0) THEN
                        position := position - 1;
                    ELSE
                        position := max_pos - 1;
                    END IF;
                END IF;
		output <= std_logic_vector(to_unsigned(position, DATA_WIDTH));
            END IF;   -- input changed 
        END IF; -- clk
    end process;
end behavior;

