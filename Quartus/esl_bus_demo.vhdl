-------------------------------------------------------------------------------
-- 
-- ESL demo
-- Version: 1.0
-- Creator: Rene Moll
-- Date: 10th April 2012
--
-------------------------------------------------------------------------------
--
-- Straight forward initialization and mapping of an IP to the avalon bus.
-- The communication part is kept simple, since only register is of interest.
--
-- Communication signals use the prefix slave_
-- User signals use the prefix user_
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity esl_bus_demo is
	generic (
		DATA_WIDTH : natural := 32;	-- word size of each input and output register
		B_RES      : natural := 8;
		LED_WIDTH  : natural := 8	-- numbers of LEDs on the DE0-NANO
		
	);
	port (
		-- signals to connect to an Avalon clock source interface
		clk			: in  std_logic;
		reset			: in  std_logic;

		-- signals to connect to an Avalon-MM slave interface
		slave_address		: in  std_logic_vector(7 downto 0);
		slave_read		: in  std_logic;
		slave_write		: in  std_logic;
		slave_readdata		: out std_logic_vector(DATA_WIDTH-1 downto 0);
		slave_writedata		: in  std_logic_vector(DATA_WIDTH-1 downto 0);
		slave_byteenable	: in  std_logic_vector((DATA_WIDTH/8)-1 downto 0);
		
		encA 			: in std_logic;
		encB 			: in std_logic;

		pwm_out			: out std_logic;
		pwm_INA			: out std_logic;
		pwm_INB			: out std_logic;


		-- signals to connect to custom user logic
		user_output		: out std_logic_vector(LED_WIDTH-1 downto 0)
	);
end esl_bus_demo;

architecture behavior of esl_bus_demo is
	-- Internal memory for the system and a subset for the IP
	signal enable : std_logic;
	signal mem        : std_logic_vector(31 downto 0);
	signal enc_out    : std_logic_vector(31 downto 0);
	signal mem_masked : std_logic_vector(LED_WIDTH-1 downto 0);

	signal pwm_dir	:  std_logic;
	signal pwm_duty_cycle :  std_logic_vector(B_RES-1 downto 0);


	component QuadratureEncoder 
    	GENERIC(
		DATA_WIDTH : natural
    	);
    	PORT (
		clk   : IN  std_logic;
		rst   : in  std_logic;
		encA  : IN  std_logic;
		encB  : IN  std_logic;
		output : OUT std_logic_vector(DATA_WIDTH-1 downto 0)
	);
	end component;

	component pwm 
    	GENERIC (
		b_resolution  : NATURAL
	);
    	PORT (
		clk  : IN STD_LOGIC;                                     -- systemclock
		rst  : IN STD_LOGIC;                                     -- async reset
		dir  : IN STD_LOGIC;                                     -- direction PWM
		duty : IN  STD_LOGIC_VECTOR(B_RES-1 DOWNTO 0);    -- duty cycle
		pwm  : OUT STD_LOGIC;                                    -- pwm output
		INA  : OUT STD_LOGIC;                                    -- Voltage control pin A
		INB  : OUT STD_LOGIC                                     -- Voltage control pin B
    	);
	end component;

begin
	-- Initialization of the example
	my_ip : QuadratureEncoder
	generic map(
		DATA_WIDTH => DATA_WIDTH
	)
	port map(
		clk    => clk,
		rst    => reset,
		encA  => encA,
		encB  => encB,
		output => enc_out
		-- output => user_output
	);

	my_pwm : pwm
	generic map (
		b_resolution => B_RES
	)
    	port map(
		clk  => clk,
		rst  => reset,
		dir => pwm_dir,
		duty => pwm_duty_cycle,
		pwm => pwm_out,
		INA => pwm_INA,
		INB => pwm_INB
    	);


	
	-- Communication with the bus
	p_avalon : process(clk, reset)
	begin
		if (reset = '1') then
			-- mem <= (others => '0');
		elsif (rising_edge(clk)) then
			if (slave_read = '1') then
				-- slave_readdata <= mem;
			end if;
			
			if (slave_write = '1') then
				mem <= slave_writedata;
			end if;
		end if;
	end process;
	
	-- Only select the amount bits that the logic can handle
	mem_masked <= mem(LED_WIDTH-1 downto 0);
	-- mem <= enc_out(DATA_WIDTH-1 downto 0);
	user_output <= enc_out(LED_WIDTH-1 downto 0);
	enable <= mem(DATA_WIDTH-1);

	pwm_dir <= mem(8);
	pwm_duty_cycle <= mem(7 downto 0);
end architecture;
