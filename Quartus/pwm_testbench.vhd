LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY pwm_testbench IS
    GENERIC (
        CLK_FREQUENCY : NATURAL := 1_000_000_000;
        B_RESOLUTION  : NATURAL := 8
    );
END pwm_testbench;

ARCHITECTURE structure OF pwm_testbench IS
    SIGNAL clk  : STD_LOGIC := '0';
    SIGNAL rst  : STD_LOGIC := '0';
    SIGNAL dir  : STD_LOGIC := '0';                                     -- direction PWM
    SIGNAL duty : STD_LOGIC_VECTOR(b_resolution-1 DOWNTO 0) := "00000000";    -- duty cycle
    SIGNAL pwm  : STD_LOGIC := '0';                                    -- pwm output
    SIGNAL INA  : STD_LOGIC := '0';                                    -- Voltage control pin A
    SIGNAL INB  : STD_LOGIC := '0';
BEGIN
    clk <= not clk after 1sec/CLK_FREQUENCY;
	 
    PROCESS
        BEGIN
		  
        duty <= std_logic_vector(to_unsigned(200, B_RESOLUTION));
		  dir <= '0';
        WAIT FOR 20 us;
		  duty <= std_logic_vector(to_unsigned(150, B_RESOLUTION));
        dir <= '1';
        WAIT FOR 10 us;
		  duty <= std_logic_vector(to_unsigned(100, B_RESOLUTION));
        dir <= '0';
        WAIT FOR 30 us;
		  duty <= std_logic_vector(to_unsigned(50, B_RESOLUTION));
        dir <= '1';
        WAIT FOR 10 us;
        ASSERT false REPORT "finished" SEVERITY note;
        WAIT;
    END PROCESS;
    
    PWM_MOD : ENTITY work.pwm(behavior)
        GENERIC MAP (
            clk_freq => CLK_FREQUENCY,
            b_resolution => B_RESOLUTION
        )
        PORT MAP ( clk => clk,                                     -- systemclock
                   rst => rst,
                   dir => dir,
                   duty => duty,
                   pwm => pwm,
                   INA => INA,
                   INB => INB
        );
END structure;