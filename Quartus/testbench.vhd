LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY testbench IS
    GENERIC (w: integer := 10);
END testbench;

ARCHITECTURE structure OF testbench IS
    SIGNAL stimulusA : std_logic;
    SIGNAL stimulusB : std_logic;
    SIGNAL resDir    : std_logic;
	 SIGNAL clock 		: std_logic;
	 SIGNAL resPos    : integer range 0 to 255;
BEGIN
	 clock <= not clock after 1 ns;
	 
    PROCESS
        BEGIN
        FOR i IN 0 TO 2**w-1 LOOP
            stimulusA <= stimulusA XOR '1';
            WAIT FOR 10 ns;
            stimulusB <= stimulusB XOR '1';
            WAIT FOR 10 ns;
        END LOOP;
        
        FOR i IN 0 TO 2**w-1 LOOP
            stimulusB <= stimulusB XOR '1';
				WAIT FOR 10 ns;
            stimulusA <= stimulusA XOR '1';
				WAIT FOR 10 ns;
        END LOOP;
        ASSERT false REPORT "finished" SEVERITY note;
        WAIT;
    END PROCESS;
    
    label_required : ENTITY work.QuadratureEncoder(behavior)
        PORT MAP (clk => clock,
                  encA => stimulusA,
                  encB => stimulusB,
                  position => resPos,
                  dir => resDir
                );
END structure;