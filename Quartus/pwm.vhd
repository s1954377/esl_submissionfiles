LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY pwm IS
    GENERIC (
        b_resolution  : NATURAL := 8
    );
    PORT (
        clk  : IN STD_LOGIC;                                     -- systemclock
        rst  : IN STD_LOGIC;                                     -- async reset
        dir  : IN STD_LOGIC;                                     -- direction PWM
        duty : IN  STD_LOGIC_VECTOR(b_resolution-1 DOWNTO 0);    -- duty cycle
        pwm  : OUT STD_LOGIC;                                    -- pwm output
        INA  : OUT STD_LOGIC;                                    -- Voltage control pin A
        INB  : OUT STD_LOGIC                                     -- Voltage control pin B
    );
END pwm;

ARCHITECTURE behavior OF pwm IS
    CONSTANT PERIOD  : NATURAL := 2**(b_resolution) - 1;
    SIGNAL   pwm_cnt : NATURAL RANGE 0 TO PERIOD;
    SIGNAL   clk_cnt : NATURAL RANGE 0 TO PERIOD;
    SIGNAL   pwm_reg : STD_LOGIC;
BEGIN
    PWM_MOD: PROCESS(clk)
    BEGIN
        if rising_edge(clk) then
            if rst = '1' then
                clk_cnt <= 0;
                pwm_cnt <= 0;
                pwm_reg <= '0';
            else
                if clk_cnt = 0 then
                    pwm_cnt <= to_integer(UNSIGNED(duty));
						  INA <= dir;      -- dir = 1 clockwise
						  INB <= NOT(dir);
                end if;
					 if clk_cnt < (PERIOD) then
						 clk_cnt <= (clk_cnt + 1);
					 else
						clk_cnt <= 0;
					 end if;
					 
--                clk_cnt <= (clk_cnt + 1) when clk_cnt <= (PERIOD - 1) else 0;
            end if;
				if clk_cnt < pwm_cnt then
					pwm_reg <= '1';
				else
					pwm_reg <= '0';
				end if;
--            pwm_reg <= '1' when clk_cnt < pwm_cnt else '0';
       end if; -- rising edge
    END PROCESS;

    pwm <= pwm_reg;

END behavior;
