/**********************************************************
 * This file is generated by 20-sim ANSI-C Code Generator
 *
 *  file:  xxinteg.c
 *  subm:  PositionControllerPan
 *  model: PositionControllerPan
 *  expmt: Jiwy-1
 *  date:  June 22, 2023
 *  time:  1:49:34 PM
 *  user:  20-sim 5.0 Student License
 *  from:  Universiteit Twente
 *  build: 5.0.2.12127
 **********************************************************/

/* This file describes the integration methods
   that are supplied for computation.

   Currently the following methods are supported:
   * Euler
   * RungeKutta2
   * RungeKutta4
   but it is easy for the user to add their own
   integration methods with these two as an example.
*/

/* the system include files */
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* our own include files */
#include "xxinteg.h"
#include "xxmodel.h"

/* global variables prototypes */
extern XXDouble xx_time;
extern XXDouble xx_step_size;

#define xx_STATE_SIZE 3

/*********************************************************************
 * Discrete integration method
 *********************************************************************/

/* the initialization of the Discrete integration method */
void XXDiscreteInitialize (void)
{
	/* nothing to be done */
	xx_major = XXTRUE;
}

/* the termination of the Discrete integration method */
void XXDiscreteTerminate (void)
{
	/* nothing to be done */
}

/* the Discrete integration method itself */
void XXDiscreteStep (void)
{
	XXInteger index;

	/* for each of the supplied states */
	for (index = 0; index < xx_STATE_SIZE; index++)
	{
		/* just a move of the new state */
		xx_s [index] = xx_R [index];
	}
	/* increment the simulation time */
	xx_time += xx_step_size;

	xx_major = XXTRUE;

	/* evaluate the dynamic part to calculate the new rates */
	XXCalculateDynamic ();
}

