/**
 * @file example.c
 * @brief c example file.
 * @author Jan Jaap Kempenaar, Sander Grimm, University of Twente.
 */

#include "stdio.h"
#include "gpmc_driver_c.h"
/* 20-sim include files */
// #include "ControllerPan/xxsubmod.h"

#include <fcntl.h>      // open()
#include <unistd.h>     // close()

void printBitString(int number){
  for (int i = sizeof(number) * 8 - 1; i>= 0; i--){
    int bit = (number >> i) & 1;
    printf("%d", bit);
  }
  printf("\n");
}

int main2(int argc, char* argv[])
{
  int fd; // File descriptor.
  if (2 != argc)
  {
    printf("Usage: %s <device_name>\n", argv[0]);
    return 1;
  }
  
  printf("GPMC driver c-example\n");
  
  // open connection to device.
  printf("Opening gpmc_fpga...\n");
  fd = open(argv[1], 0);
  if (0 > fd)
  {
    printf("Error, could not open device: %s.\n", argv[1]);
    return 1;
  }
  
  // define the (16-bit) variable to send/receive
  unsigned short value0 = 100;
  unsigned short value1 = 100;
  unsigned short value2 = 100;
  unsigned short value3 = 100;



//   XXDouble u [2 + 1];
//   XXDouble y [2 + 1];

//   /* Initialize the inputs and outputs with correct initial values */
//   u[0] = 0.0;		/* in */
//   u[1] = 0.0;		/* position */

//   y[0] = 0.0;		/* corr */
//   y[1] = 0.0;		/* out */


  while(1){
    value0 = getGPMCValue(fd, 0);
    value1 = getGPMCValue(fd, 1);
    printf("read back : Pan: %i and Tilt: %i \n", value0, value1);
    usleep(1000000);

    int pwmA = 0 + 30;//32768
    // printBitString(pwmA);

    setGPMCValue(fd, pwmA, 3);
    value2 = getGPMCValue(fd, 2);
    printBitString(value2);

    usleep(1000000);
    pwmA += 32768;
    setGPMCValue(fd, pwmA, 3);

    printf("PWM: %i\n", value2);
  }
  

  printf("Exiting...\n");
  // close connection to free resources.
  close(fd);
  return 0;

	


	// /* Initialize the submodel itself */
	// XXInitializeSubmodel (u, y, xx_time);

	// /* Simple loop, the time is incremented by the integration method */
	// while ( (xx_time < xx_finish_time) && (xx_stop_simulation == XXFALSE) )
	// {
	// 	/* Call the submodel to calculate the output */
	// 	XXCalculateSubmodel (u, y, xx_time);
	// }

	// /* Perform the final calculations */
	// XXTerminateSubmodel (u, y, xx_time);

	// /* and we are done */
	// return 0;
}


