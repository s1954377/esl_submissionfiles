/**
 * @file example.c
 * @brief c example file.
 * @author Jan Jaap Kempenaar, Sander Grimm, University of Twente.
 */

#include "stdio.h"
#include <stdint.h>
#include "gpmc_driver_c.h"

#include <fcntl.h>      // open()
#include <unistd.h>     // close()
#include <sys/time.h>

#include "xxsubmod.h" 

XXDouble u [2 + 1];
XXDouble y [2 + 1];

void initializeController(){
 	/* Initialize the inputs and outputs with correct initial values */
	u[0] = 0.0;		/* in */
	u[1] = 0.0;		/* position */

	y[0] = 0.0;		/* corr */
	y[1] = 0.0;		/* out */
  	/* Initialize the submodel itself */
  printf("[DEBUG] Initialze submodel");
	XXInitializeSubmodel (u, y, xx_time);
}

XXDouble convertToRadians(int value){
  return (float)value/4108.0 * 5/3 * 3.1415926535;
}

struct timeval currentTime;
long getMicroTime(){
  gettimeofday(&currentTime, NULL);
  return currentTime.tv_sec * 1000000 + currentTime.tv_usec;
}

void printBitString(int number){
  for (int i = sizeof(number) * 8 - 1; i>= 0; i--){
    int bit = (number >> i) & 1;
    printf("%d", bit);
  }
  printf("\n");
}

int goToHomingStation(int fd, int index){
  setGPMCValue(fd, -50, index + 2);
  int prevEncValue = -1;
  int encValue = getGPMCValue(fd, index);
  usleep(100000);

  
  
  while (prevEncValue != encValue){
    prevEncValue = encValue;
    encValue = getGPMCValue(fd, index);
    usleep(10000);
  }

  setGPMCValue(fd, 0, index + 2);
  printf("Encoder Value: %i\n", getGPMCValue(fd, index));
  return encValue;
}

int main(int argc, char* argv[])
{
  int fd; // File descriptor.
  if (2 != argc)
  {
    printf("Usage: %s <device_name>\n", argv[0]);
    return 1;
  }
  
  // open connection to device.
  printf("Opening gpmc_fpga...\n");
  fd = open(argv[1], 0);
  if (0 > fd)
  {
    printf("Error, could not open device: %s.\n", argv[1]);
    return 1;
  }

  int homePosition = goToHomingStation(fd, 0);
  initializeController();
  int setpoint = 1;
  int period = 20000;
  long currTime, prevTime;

  int i = 0;

  /* Simple loop, the time is incremented by the integration method */
  while (1){
    currTime = getMicroTime();
    if(currTime >= prevTime + period){
      prevTime = currTime;
      i++;
      if (i >= 50){
        setpoint *= -1;
        i = 0;
      }

      u[0] = setpoint;
      u[1] = convertToRadians(getGPMCValue(fd,0) - homePosition);// homePosition);
      /* Call the submodel to calculate the output */
      XXCalculateSubmodel (u, y, xx_time);

      //need to correctly map out of controller to PWM
      int16_t PWM = y[1]*40;
      // printBitString(PWM);
      setGPMCValue(fd, PWM, 2);
      printf("Input:  [Setp]: %lf [Pos]: %lf\tOutput: [Corr]: %lf [Out]: %lf\n", u[0], u[1], y[0], y[1]);
    }
  }

  printf("Exiting...\n");
  // close connection to free resources.
  close(fd);

	/* Perform the final calculations */
	XXTerminateSubmodel (u, y, xx_time);
  return 0;
}

