#include <gst/gst.h>
#include <glib.h>

typedef struct _CustomData {
  // GstElement *pipeline, *app_source, *tee, *audio_queue, *audio_convert1, *audio_resample, *audio_sink;
  // GstElement *pipeline, *source, *encoder, *decoder,  *sink, *video_cap;
  GstElement *sink;
  // guint64 num_samples;   /* Number of samples generated so far (for timestamp generation) */
  // gfloat a, b, c, d;     /* For waveform generation */

  // guint sourceid;        /* To control the GSource */

  // GMainLoop *main_loop;  /* GLib's Main Loop */
} CustomData;

static gboolean
bus_call (GstBus     *bus,
          GstMessage *msg,
          gpointer    data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {

    case GST_MESSAGE_EOS:
      g_print ("End of stream\n");
      g_main_loop_quit (loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar  *debug;
      GError *error;

      gst_message_parse_error (msg, &error, &debug);
      g_free (debug);

      g_printerr ("Error: %s\n", error->message);
      g_error_free (error);

      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }

  return TRUE;
}


static void
on_pad_added (GstElement *element,
              GstPad     *pad,
              gpointer    data)
{
  GstPad *sinkpad;
  GstElement *decoder = (GstElement *) data;

  /* We can now link this pad with the vorbis-decoder sink pad */
  g_print ("Dynamic pad created, linking demuxer/decoder\n");

  sinkpad = gst_element_get_static_pad (decoder, "sink");

  gst_pad_link (pad, sinkpad);

  gst_object_unref (sinkpad);
}

static GstFlowReturn new_sample_callback (GstElement *sink, CustomData *data) {
  GstSample *sample;

  /* Retrieve the buffer */
  g_signal_emit_by_name (sink, "pull-sample", &sample);
  if (sample) {
    GstBuffer *buffer = gst_sample_get_buffer(sample);
    GstMapInfo info;
    gst_buffer_map(buffer, &info, GST_MAP_READ);
    g_print("Data: %.*s \n", (int)info.size, (gchar *) info.data);
    gst_buffer_unmap(buffer, &info);
    /* The only thing we do in this example is print a * to indicate a received buffer */
    gst_sample_unref (sample);
    return GST_FLOW_OK;
  }

  return GST_FLOW_ERROR;
}


int
main (int   argc,
      char *argv[])
{
  CustomData data;
  GMainLoop *loop;
  GstCaps *caps;

  GstElement *pipeline, *source, *encoder, *decoder,  *sink, *video_cap;
  GstBus *bus;
  guint bus_watch_id;
  /* Initialisation */
  gst_init (&argc, &argv);

  loop = g_main_loop_new (NULL, FALSE);


  /* Check input arguments */
  if (argc != 3) {
    g_printerr ("Usage: %s <Ogg/Vorbis filename>\n", argv[0]);
    return -1;
  }


  /* Create gstreamer elements */
  pipeline = gst_pipeline_new ("video-player");
  source   = gst_element_factory_make ("v4l2src",  "video-source");
  encoder  = gst_element_factory_make ("jpegenc",  "jpeg-encoder");
  video_cap= gst_element_factory_make ("capsfilter", "video-cap");
  decoder  = gst_element_factory_make ("jpegdec",   "jpeg-decoder");
  data.sink     = gst_element_factory_make ("appsink", "app-sink");

  if (!pipeline || !source || !video_cap || !encoder || !decoder || !data.sink) {
    g_printerr ("One element could not be created. Exiting.\n");
    return -1;
  }

  /* Set up the pipeline */
  caps = gst_caps_new_simple("image/jpeg",
                             "width", G_TYPE_INT, 160,
                             "height", G_TYPE_INT, 120,
                             "framerate", GST_TYPE_FRACTION, 30, 1,
                             NULL
  );


  /* we set the input filename to the source element */
  g_object_set (G_OBJECT (source), "device", argv[1], NULL);
  g_object_set (G_OBJECT (video_cap), "caps", caps, NULL);


  /* config appsink */
  g_object_set (G_OBJECT (data.sink), "emit-signals", TRUE, "sync", FALSE, NULL);
  g_signal_connect(data.sink, "new-sample", G_CALLBACK(new_sample_callback), &data);





  /* we add a message handler */
  bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
  bus_watch_id = gst_bus_add_watch (bus, bus_call, loop);
  gst_object_unref (bus);

  /* we add all elements into the pipeline */
  /* file-source | ogg-demuxer | vorbis-decoder | converter | alsa-output */
  gst_bin_add_many (GST_BIN (pipeline),
                    source, encoder, video_cap, decoder, data.sink, NULL);

  /* we link the elements together */
  /* file-source -> ogg-demuxer ~> vorbis-decoder -> converter -> alsa-output */
  gst_element_link_many (source, encoder, video_cap, decoder, data.sink, NULL);

  // g_signal_connect (encoder, "pad-added", G_CALLBACK (on_pad_added), decoder);

  /* note that the demuxer will be linked to the decoder dynamically.
     The reason is that Ogg may contain various streams (for example
     audio and video). The source pad(s) will be created at run time,
     by the demuxer when it detects the amount and nature of streams.
     Therefore we connect a callback function which will be executed
     when the "pad-added" is emitted.*/


  /* Set the pipeline to "playing" state*/
  g_print ("Now process VideoStream: %s\n", argv[1]);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);


  /* Iterate */
  g_print ("Running...\n");
  g_main_loop_run (loop);


  /* Out of the main loop, clean up nicely */
  g_print ("Returned, stopping playback\n");
  gst_element_set_state (pipeline, GST_STATE_NULL);

  g_print ("Deleting pipeline\n");
  gst_object_unref (GST_OBJECT (pipeline));
  g_source_remove (bus_watch_id);
  g_main_loop_unref (loop);

  return 0;
}
