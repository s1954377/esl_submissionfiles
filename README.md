# ESL_submissionFiles
This repository contains submission files for the Embedded Systems Laboratory course of quartile 2022-2B.
These files are created by Stijn Brugman (s2359413) and Nils Rutgers (s1954377).

The Controller_Communication folder contains the provided code for GPMC communication, this is combined with the 20-sim generated C code for the controller. The combined script can be found in the directory example_code/src/example_c.c.
The ImageProcessing folder contains the files needed for the Gstreamer pipeline. This pipeline is able to store a camera feed in YUV format.
The Quartus folder contains a Quartus project combining the aspects of assignments 3-6 of the course. This includes the esl_bus_demo, Encoder, PWM and the relevant testbenches. 